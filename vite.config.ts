import { resolve } from 'path'
import vue from '@vitejs/plugin-vue'
import Icons from 'unplugin-icons/vite'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { ConfigEnv, UserConfigExport } from 'vite'
import PkgConfig from 'vite-plugin-package-config'
import AutoImport from 'unplugin-auto-import/vite'
import IconsResolver from 'unplugin-icons/resolver'
import Components from 'unplugin-vue-components/vite'
import VueSetupExtend from 'vite-plugin-vue-setup-extend'
import { FileSystemIconLoader } from 'unplugin-icons/loaders'
// import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import OptimizationPersist from 'vite-plugin-optimize-persist'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

const pathResolve = (dir: string): string => resolve(__dirname, '.', dir)

export default ({ mode }: ConfigEnv): UserConfigExport => {
  const __DEV__ = mode === 'development'

  // @ts-ignore
  return {
    resolve: {
      alias: {
        '@': pathResolve('src')
      }
    },
    plugins: [
      vue(),
      vueJsx(),
      VueSetupExtend(),
      AutoImport({
        dts: 'types/auto-imports.d.ts',
        resolvers: [ElementPlusResolver()],
        imports: ['vue']
      }),
      Components({
        dts: 'types/components.d.ts',
        resolvers: [
          ElementPlusResolver(),
          IconsResolver({
            enabledCollections: ['ep'],
            customCollections: ['local']
          })
        ]
      }),
      Icons({
        compiler: 'vue3',
        autoInstall: true,
        customCollections: {
          local: FileSystemIconLoader(pathResolve('src/assets/svgs'), svg =>
            svg.replace(/^<svg /, '<svg fill="currentColor" ')
          )
        }
      }),
      /*createSvgIconsPlugin({
        iconDirs: [pathResolve('src/assets/icons/')],
        symbolId: 'icon-[dir]-[name]'
      }),*/
      PkgConfig(),
      OptimizationPersist()
    ],
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@use "@/styles/element/index.scss" as *;`
        }
      }
    },
    build: {
      minify: 'terser',
      sourcemap: __DEV__,
      brotliSize: false,
      chunkSizeWarningLimit: 2000,
      terserOptions: {
        compress: {
          drop_console: !__DEV__,
          drop_debugger: !__DEV__
        }
      },
      rollupOptions: {
        output: {
          chunkFileNames: 'js/[name]-[hash].js',
          entryFileNames: 'js/[name]-[hash].js',
          assetFileNames: '[ext]/[name]-[hash].[ext]'
        }
      }
    },
    server: {
      hmr: { overlay: false },
      https: false,
      port: 3000,
      host: '0.0.0.0',
      cors: true,
      proxy: {
        '/api': {
          target: 'http://localhost:8080',
          changeOrigin: true,
          rewrite: (path: string) => path.replace(/^\/api/, '')
        }
      }
    }
  }
}
