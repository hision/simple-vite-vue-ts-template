import type { App } from 'vue'

declare global {
  interface Window {
    __APP__: App<Element>
  }

  type Nullable<T> = T | null

  type BaseTypeCode =
    | 'person'
    | 'organization'
    | 'device'
    | 'deviceUnit'
    | 'propertyBase'
    | 'deviceUnitGroup'
    | 'deviceUnitProperty'
    | 'alarmConfig'
    | 'eventConfig'
    | 'eventAlarmConfig'
}
