import 'vue-router'

declare module 'vue-router' {
  interface RouteMeta {
    // default env.VITE_APP_TITLE
    title?: string
    // default true
    auth?: boolean
    // default false
    keepAlive?: boolean
    // default unknown
    activeMenu?: string
    // default ''
    icon?: string
    // default false
    breadcrumb?: boolean
    // default fade
    transition?: 'fade' | 'slide-left'
    // default false
    hideTab?: boolean
    // default true
    sidebar?: boolean
  }
}
