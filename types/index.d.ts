interface UserInfo {
  access_token: string
  token_type: string
  refresh_token: string
  expires_in: string
  scope: 'all'
  user_id: string
  username: string
  client_id: string
  jti: string
  timestamp: number
}

interface Menu {
  icon: string
  name: string
  orderNum: number
  innerRoute: boolean
  outUrl: Nullable<string>
  path: string
  target: '_self'
  sidebar: boolean
  children?: Menu[]
}

interface MenusInfo {
  menus: Menu[]
  baseTypes: { id: string; baseTypeName: string; code: BaseTypeCode; sort: number }[]
}

interface AppMainTab {
  route: string
  name: string
  title: string
}

interface PageBeanSearch<T extends { [keys: string]: any }> {
  query: T
  size: number
  current: number
  orderRule?: string
}

interface PageResult<T extends { [keys: string]: any }> {
  records: T[]
  total: number
  size: number
  current: number
}
