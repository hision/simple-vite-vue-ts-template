module.exports = {
  printWidth: 100,
  tabWidth: 2,
  semi: false,
  bracketSpacing: true,
  singleQuote: true,
  arrowParens: 'avoid',
  trailingComma: 'none',
  htmlWhitespaceSensitivity: 'strict'
}
