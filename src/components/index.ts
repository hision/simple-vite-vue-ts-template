import type { App } from 'vue'
import MyEditor from './myEditor/index.vue'
import SvgIcon from './svgIcon/index.vue'

export default {
  install: (app: App<Element>) => {
    return app.component(SvgIcon.name, SvgIcon).component(MyEditor.name, MyEditor)
  }
}
