import type { RouteRecordRaw } from 'vue-router'
import Layout from '@/layout/index.vue'
import newsRoutes from '@/routes/news'

// will generateMenus
export const constantRoutes: RouteRecordRaw[] = [...newsRoutes]

const defaultRoutes: RouteRecordRaw[] = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login.vue'),
    meta: {
      title: '登录',
      auth: false,
      hideTab: true
    }
  },
  {
    path: '/',
    component: Layout,
    meta: {
      title: '首页'
    },
    redirect: '/home',
    children: [
      {
        path: '/home',
        name: 'Home',
        component: () => import('@/views/admin/Home.vue'),
        meta: {
          title: '首页'
        }
      }
    ]
  },
  {
    path: '/403',
    name: '403',
    component: () => import('@/views/error/403.vue'),
    meta: {
      auth: false
    }
  },
  {
    path: '/:patchMatch(.*)*',
    name: '404',
    component: () => import('@/views/error/404.vue'),
    meta: {
      auth: false
    }
  }
]

const routes: RouteRecordRaw[] = [...defaultRoutes, ...constantRoutes]

export const generateMenus = () => constantRoutes.map(br => recursionRoute(br))

const recursionRoute = (route: RouteRecordRaw): Menu => ({
  icon: '',
  name: route.meta!.title as string,
  path: route.path,
  innerRoute: true,
  outUrl: null,
  target: '_self',
  sidebar: route.meta?.sidebar as boolean,
  children:
    route.children && route.children.length ? route.children.map(r => recursionRoute(r)) : [],
  orderNum: 1
})

export default routes
