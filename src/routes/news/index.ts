import type { RouteRecordRaw } from 'vue-router'
import Layout from '@/layout/index.vue'

const newsRoutes: RouteRecordRaw[] = [
  {
    path: '/news',
    component: Layout,
    meta: {
      title: '新闻管理'
    },
    children: [
      {
        path: '/news/page',
        name: 'NewsPage',
        component: () => import('@/views/news/page/index.vue'),
        meta: {
          title: '新闻列表',
          icon: 'ic ic-homepage-fill',
          breadcrumb: true,
          keepAlive: true
        }
      },
      {
        path: '/news/type',
        name: 'NewsType',
        component: () => import('@/views/news/type/NewsType.vue'),
        meta: {
          title: '新闻类型',
          icon: 'ic ic-homepage-fill',
          breadcrumb: true
        }
      }
    ]
  }
]

export default newsRoutes
