import type { App, Directive } from 'vue'
import resize from './resize'

const directives = { resize }

export default {
  install: (app: App<Element>) => {
    Object.keys(directives).forEach(key => {
      app.directive(key, (directives as { [key: string]: Directive })[key])
    })
    return app
  }
}
