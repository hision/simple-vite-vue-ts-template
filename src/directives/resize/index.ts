import type { DirectiveBinding, Directive, VNode } from 'vue'
import type { Erd } from 'element-resize-detector'
import { emitter } from '@/utils/mitt'
import elementResizeDetectorMaker from 'element-resize-detector'

const erd: Erd = elementResizeDetectorMaker({
  strategy: 'scroll'
})

const resize: Directive = {
  mounted(el: HTMLElement, binding?: DirectiveBinding, vnode?: VNode) {
    erd.listenTo(el, elem => {
      const width = elem.offsetWidth
      const height = elem.offsetHeight
      if (binding?.instance) {
        emitter.emit('resize', { detail: { width, height } })
      } else {
        // @ts-ignore
        vnode?.el.dispatchEvent(new CustomEvent('resize', { detail: { width, height } }))
      }
    })
  },
  unmounted(el: HTMLElement) {
    erd.uninstall(el)
  }
}

export default resize
