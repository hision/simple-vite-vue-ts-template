export const storage = {
  set(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value))
  },
  get<T>(key: string): Nullable<T> {
    const value = localStorage.getItem(key)
    return value && value !== 'undefined' && value !== 'null' ? <T>JSON.parse(value) : null
  },
  remove(key: string) {
    localStorage.removeItem(key)
  }
}

/**
 * 封装操作sessionStorage本地存储的方法
 */
export const sessionStorage = {
  set(key: string, value: any): void {
    window.sessionStorage.setItem(key, JSON.stringify(value))
  },
  get<T>(key: string): Nullable<T> {
    const value = window.sessionStorage.getItem(key)
    return value && value !== 'undefined' && value !== 'null' ? <T>JSON.parse(value) : null
  },
  remove(key: string): void {
    window.sessionStorage.removeItem(key)
  }
}
