import NProgress from '@/utils/nprogress'
import { useUserOutsideStore } from '@/store/user'
import axios, { type AxiosRequestConfig } from 'axios'
import { refreshTokenAPI } from '@/apis/adminAPI'
import { ElMessage } from 'element-plus'

const axiosInstance = axios.create({
  baseURL: '/api',
  timeout: 1000,
  headers: {
    'Content-Type': 'application/json; charset=UTF-8'
  }
})

axiosInstance.interceptors.request.use((config): AxiosRequestConfig => {
  const { tokenType, accessToken } = useUserOutsideStore()
  if (accessToken) {
    config.headers!.Authorization = `${tokenType} ${accessToken}`
  }
  return config
})

enum HttpCode {
  TOKEN_EXPIRED = '00002',
  TOKEN_INVALID_OR_EXPIRED = 'A0230'
}

const httpState = {
  isRefreshToken: false
}

let requests: Function[] = []

axiosInstance.interceptors.response.use(
  async res => {
    const config = res.config
    const { status, code, message } = res.data as ResType
    switch (code) {
      case HttpCode.TOKEN_EXPIRED: {
        if (!httpState.isRefreshToken) {
          httpState.isRefreshToken = true
          const userStore = useUserOutsideStore()
          let userInfo
          try {
            userInfo = await refreshTokenAPI(userStore.refreshToken)
          } catch (e) {
            return Promise.reject(e)
          } finally {
            httpState.isRefreshToken = false
          }
          userStore.setUserInfo(
            Object.assign(userInfo, {
              timestamp: new Date().getTime()
            })
          )
          requests.forEach(cb => cb())
          requests = []
          return axiosInstance(config)
        } else {
          return new Promise(resolve => {
            requests.push(() => {
              resolve(axiosInstance(config))
            })
          })
        }
      }
      default: {
        if (!status) {
          ElMessage.error({ message })
        }
        break
      }
    }
    return Promise.resolve(res.data)
  },
  error => Promise.reject(error)
)

interface ResType<T = unknown> {
  status: boolean
  code: Partial<HttpCode>
  data: T
  message: string
}

const http = {
  get<T>(url: string, params?: unknown): Promise<T> {
    return new Promise((resolve, reject) => {
      NProgress.start()
      axiosInstance
        .get<T, ResType<T>>(url, { params })
        .then(res => resolve(res.data))
        .catch(err => reject(err))
        .finally(() => NProgress.done())
    })
  },
  post<T>(url: string, data?: unknown, config?: AxiosRequestConfig): Promise<T> {
    return new Promise((resolve, reject) => {
      NProgress.start()
      axiosInstance
        .post<T, ResType<T>>(url, data, config)
        .then(res => resolve(res.data))
        .catch(err => reject(err))
        .finally(() => NProgress.done())
    })
  },
  upload<T>(url: string, file: unknown): Promise<T> {
    return new Promise((resolve, reject) => {
      NProgress.start()
      axiosInstance
        .post<T, ResType<T>>(url, file, {
          headers: { 'Content-Type': 'multipart/form-data' }
        })
        .then(res => resolve(res.data))
        .catch(err => reject(err))
        .finally(() => NProgress.done())
    })
  },
  download(url: string): void {
    const iframe = document.createElement('iframe')
    iframe.style.display = 'none'
    iframe.src = url
    iframe.onload = function () {
      document.body.removeChild(iframe)
    }
    document.body.appendChild(iframe)
  }
}

export { axiosInstance }

export default http
