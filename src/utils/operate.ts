export const hasClass = (ele: HTMLElement, cls: string) => {
  return !!ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'))
}

export const addClass = (ele: HTMLElement, cls: string, extraCls: string) => {
  if (!hasClass(ele, cls)) ele.className += ' ' + cls
  if (extraCls) {
    if (!hasClass(ele, extraCls)) ele.className += ' ' + extraCls
  }
}

export const removeClass = (ele: HTMLElement, cls: string, extraCls: string) => {
  if (hasClass(ele, cls)) {
    const reg = new RegExp('(\\s|^)' + cls + '(\\s|$)')
    ele.className = ele.className.replace(reg, ' ').trim()
  }
  if (extraCls) {
    if (hasClass(ele, extraCls)) {
      const regs = new RegExp('(\\s|^)' + extraCls + '(\\s|$)')
      ele.className = ele.className.replace(regs, ' ').trim()
    }
  }
}

export const toggleClass = (flag: boolean, clsName: string, target: Element) => {
  const targetEl = target || document.body
  let { className } = targetEl
  className = className.replace(clsName, '')
  targetEl.className = flag ? `${className} ${clsName} ` : className
}
