import http from '@/utils/http'

export function loginAPI(loginForm: { username: string; password: string }) {
  const { username, password } = loginForm
  return http.post<UserInfo>('/oauth/token', null, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    params: {
      username,
      password,
      grant_type: 'password',
      client_id: 'wav-admin-server',
      client_secret: '123456'
    }
  })
}

export function refreshTokenAPI(refresh_token: string) {
  return http.post<UserInfo>('/oauth/token', null, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    params: {
      refresh_token,
      grant_type: 'refresh_token',
      client_id: 'wav-admin-server',
      client_secret: '123456'
    }
  })
}

export function menusAPI() {
  return http.get<MenusInfo>('/api/admin/auth/getMenus')
}
