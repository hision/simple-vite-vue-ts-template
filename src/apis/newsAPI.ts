import http from '@/utils/http'

export function newsPagingAPI(pageSearch: PageBeanSearch<any>) {
  return http.post<PageResult<any>>('/news/paging', pageSearch)
}
