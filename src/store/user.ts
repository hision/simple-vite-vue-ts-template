import { defineStore } from 'pinia'
import { storage } from '@/utils/storage'
import { pinia } from '@/plugins'

export const useUserStore = defineStore('user', {
  state: () => ({
    accessToken:
      (storage.get<UserInfo>('userInfo') && storage.get<UserInfo>('userInfo')!.access_token) || '',
    tokenType:
      (storage.get<UserInfo>('userInfo') && storage.get<UserInfo>('userInfo')!.token_type) || '',
    refreshToken:
      (storage.get<UserInfo>('userInfo') && storage.get<UserInfo>('userInfo')!.refresh_token) || '',
    expiresIn:
      (storage.get<UserInfo>('userInfo') && storage.get<UserInfo>('userInfo')!.expires_in) || 7200,
    clientId:
      (storage.get<UserInfo>('userInfo') && storage.get<UserInfo>('userInfo')!.client_id) || '',
    username:
      (storage.get<UserInfo>('userInfo') && storage.get<UserInfo>('userInfo')!.username) || '',
    jti: (storage.get<UserInfo>('userInfo') && storage.get<UserInfo>('userInfo')!.jti) || '',
    timestamp:
      (storage.get<UserInfo>('userInfo') && storage.get<UserInfo>('userInfo')!.timestamp) ||
      new Date().getTime()
  }),
  actions: {
    setUserInfo(userInfo: Partial<UserInfo>) {
      storage.set('userInfo', userInfo)
      const {
        access_token,
        token_type,
        refresh_token,
        expires_in,
        client_id,
        username,
        jti,
        timestamp
      } = userInfo
      this.accessToken = access_token || ''
      this.tokenType = token_type || ''
      this.refreshToken = refresh_token || ''
      this.expiresIn = expires_in || 7200
      this.clientId = client_id || ''
      this.username = username || ''
      this.jti = jti || ''
      this.timestamp = timestamp || new Date().getTime()
    },
    clearUserInfo() {
      storage.remove('userInfo')
      this.accessToken = ''
      this.tokenType = ''
      this.refreshToken = ''
      this.expiresIn = 0
      this.clientId = ''
      this.username = ''
      this.jti = ''
      this.timestamp = new Date().getTime()
    }
  }
})

export const useUserOutsideStore = () => useUserStore(pinia)
