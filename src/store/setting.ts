import { defineStore } from 'pinia'
import { pinia } from '@/plugins'

export const useSettingStore = defineStore('setting', {
  state: () => ({
    openedTabs: [{ route: '/home', title: '首页', name: 'Home' }] as AppMainTab[],
    currentRoute: '/home'
  }),
  actions: {
    addTab(tab: AppMainTab) {
      this.openedTabs.push(tab)
    },
    deleteTab(route: string) {
      const index = this.openedTabs.findIndex(tab => tab.route === route)
      this.openedTabs.splice(index, 1)
    },
    setTab(route: string) {
      this.currentRoute = route
    },
    clearTab() {
      this.openedTabs = [{ route: '/home', title: '首页', name: 'Home' }]
    }
  }
})

export const useSettingOutsideStore = () => useSettingStore(pinia)
