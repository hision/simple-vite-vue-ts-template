import { createApp } from 'vue'
import App from './App.vue'
import components from './components'
import directives from './directives'
import plugins, { router } from './plugins'
import '@/styles/index.scss'
import 'element-plus/es/components/message/style/css'
// 加载 SVG 图标
// import 'virtual:svg-icons-register'

const app = createApp(App)

;(async () => {
  app.use(components).use(directives).use(plugins)
  await router.isReady()
  app.mount('#app')
})()
