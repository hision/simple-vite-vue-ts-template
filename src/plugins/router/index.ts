import routes from '@/routes'
import { useUserOutsideStore } from '@/store/user'
import NProgress from '@/utils/nprogress'
import { createRouter, createWebHashHistory, Router } from 'vue-router'
import { useSettingOutsideStore } from '@/store/setting'

export const router: Router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  NProgress.start()
  const { title, auth, hideTab } = to.meta
  document.title = title || import.meta.env.VITE_APP_TITLE
  const { openedTabs, addTab, setTab } = useSettingOutsideStore()
  if (!hideTab && openedTabs.findIndex(tab => tab.route === to.path) === -1) {
    addTab({ route: to.path, title: title || '', name: (to.name || '') as string })
  }
  setTab(to.path)
  if (typeof auth === 'boolean' && !auth) {
    next()
  } else {
    const { accessToken } = useUserOutsideStore()
    if (accessToken) next()
    else next('/login')
  }
})

router.afterEach(() => NProgress.done())

export default router
